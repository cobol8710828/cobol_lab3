       IDENTIFICATION DIVISION.
       PROGRAM-ID. Data4.
       AUTHOR. Kiirati.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  STUDENT-REC-DATA  PIC   x(44) VALUE "1205621WIlliam  fitzpatr
       -   "ick19751021LM05138".
       01  LONG-STR PIC   X(200)   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
       -   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
       -   "XXXXXXXXXX".
       01  STUDENT-REC PIC   X(44).
           05 STUDENT-ID  PIC 9(7).
           05 STUDENT-NAME  PIC X(21).
              10 FORE-NAME   PIC   X(9).
              10 SURNAME.
                 15 F-SURNAME   PIC   x.
                 15 FILLER   PIC   X(11).
           05 DATE-OF-Birth  PIC X(8).
              8 YOB   PIC   9(4).
              8  MOB-DOB.
                 10 MOB   PIC   99.
                 10 DOB   PIC   99.
           05 COURSE-ID  PIC X(5).
           05 GPA  PIC 9V99.
       PROCEDURE DIVISION.
           DISPLAY STUDENT-REC-DATA 
           MOVE STUDENT-REC-DATA TO STUDENT-REC.
           DISPLAY  STUDENT-REC
           DISPLAY  STUDENT-ID
           DISPLAY  STUDENT-NAME
           DISPLAY  FORE-NAME
           DISPLAY  SURNAME
           DISPLAY  F-SURNAME   "."   FORE-NAME
           DISPLAY  DATE-OF-Birth
           DISPLAY  DOB"/"MOB"/"YOB
           DISPLAY  COURSE-ID
           DISPLAY  GPA
           DISPLAY  MOB-DOB
           .